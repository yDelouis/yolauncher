rootProject.name = "YoLauncher"

include(":app")

dependencyResolutionManagement {
    versionCatalogs {
        create("libs") {

            version("kotlin", "1.8.20")

            library("android-gradle", "com.android.tools.build:gradle:8.0.0")

            library("androidx-appcompat", "androidx.appcompat:appcompat:1.2.0")
            library("androidx-recyclerview", "androidx.recyclerview:recyclerview:1.1.0")
            library("androidx-ktx", "androidx.core:core-ktx:1.3.2")
            library("androidx-fragmentKtx", "androidx.fragment:fragment-ktx:1.3.2")
            library("androidx-constraintlayout", "androidx.constraintlayout:constraintlayout:2.0.4")
            library("androidx-activityCompose", "androidx.activity:activity-compose:1.7.0")

            version("androidx-lifecycle", "2.2.0")
            library("androidx-lifecycle-runtimeKtx", "androidx.lifecycle:lifecycle-runtime-ktx:2.3.1")
            library("androidx-lifecycle-extensions", "androidx.lifecycle", "lifecycle-extensions").versionRef("androidx-lifecycle")
            library("androidx-lifecycle-viewmodelKtx", "androidx.lifecycle", "lifecycle-viewmodel-ktx").versionRef("androidx-lifecycle")

            version("room", "2.5.1")
            library("room-runtime", "androidx.room", "room-runtime").versionRef("room")
            library("room-compiler", "androidx.room", "room-compiler").versionRef("room")
            library("room-ktx", "androidx.room", "room-ktx").versionRef("room")

            library("google-material", "com.google.android.material:material:1.8.0")

            version("hilt", "2.45")
            library("hilt-gradle", "com.google.dagger", "hilt-android-gradle-plugin").versionRef("hilt")
            library("hilt-android", "com.google.dagger", "hilt-android").versionRef("hilt")
            library("hilt-compiler", "com.google.dagger", "hilt-android-compiler").versionRef("hilt")

            version("compose", "1.4.1")
            version("compose-compiler", "1.4.6")
            library("compose-layout", "androidx.compose.foundation", "foundation-layout").versionRef("compose")
            library("compose-ui", "androidx.compose.ui", "ui").versionRef("compose")
            library("compose-uiUtil", "androidx.compose.ui", "ui-util").versionRef("compose")
            library("compose-runtime", "androidx.compose.runtime", "runtime").versionRef("compose")
            library("compose-runtimeLivedata", "androidx.compose.runtime", "runtime-livedata").versionRef("compose")
            library("compose-material", "androidx.compose.material", "material").version("1.3.1")
            library("compose-animation", "androidx.compose.animation", "animation").versionRef("compose")
            library("compose-tooling", "androidx.compose.ui", "ui-tooling").versionRef("compose")
            library("compose-iconsExtended", "androidx.compose.material", "material-icons-extended").versionRef("compose")

            library("junit", "junit:junit:4.13.2")
        }
    }
}
