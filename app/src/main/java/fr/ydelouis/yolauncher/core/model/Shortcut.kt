package fr.ydelouis.yolauncher.core.model

import android.graphics.drawable.Drawable

data class Shortcut(
    val id: String,
    val title: CharSequence?,
    val icon: Drawable,
    val packageName: String
)