package fr.ydelouis.yolauncher.core.model

import androidx.lifecycle.LiveData

interface AppRepository {

    fun getLiveLauncherApps(): LiveData<List<App>>

    suspend fun save(app: App)
}
