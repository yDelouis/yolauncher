package fr.ydelouis.yolauncher.core.model

import android.content.ComponentName
import android.graphics.drawable.Drawable

data class App(
    val name: CharSequence,
    val icon: Drawable,
    val componentName: ComponentName,
    val isVisible: Boolean
) {
    val packageName: String = componentName.packageName
    val activityName: String = componentName.className

    override fun hashCode(): Int = componentName.hashCode()

    override fun equals(other: Any?) = componentName == (other as? App)?.componentName
}
