package fr.ydelouis.yolauncher.core.model

import androidx.lifecycle.LiveData

interface ShortcutRepository {

    fun getLiveAppShortcuts(packageName: String): LiveData<List<Shortcut>?>
}
