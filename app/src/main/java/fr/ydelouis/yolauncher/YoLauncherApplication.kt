package fr.ydelouis.yolauncher

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class YoLauncherApplication : Application()
