package fr.ydelouis.yolauncher.ui.home

import android.app.Application
import android.content.pm.LauncherApps
import android.os.Build
import android.os.Process
import androidx.annotation.RequiresApi
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import fr.ydelouis.yolauncher.core.model.App
import fr.ydelouis.yolauncher.core.model.AppRepository
import fr.ydelouis.yolauncher.core.model.Shortcut
import fr.ydelouis.yolauncher.core.model.ShortcutRepository
import fr.ydelouis.yolauncher.util.combineWith
import fr.ydelouis.yolauncher.util.liveDataOf
import kotlinx.coroutines.launch
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    application: Application,
    private val appRepository: AppRepository,
    shortcutRepository: ShortcutRepository,
    private val launcherApps: LauncherApps,
) : AndroidViewModel(application) {

    private val selectedApp = MutableLiveData<App?>()
    val shortcuts: LiveData<AppAndShortcuts?> = selectedApp.switchMap { selectedApp ->
        selectedApp?.let { app ->
            shortcutRepository.getLiveAppShortcuts(app.packageName).map { AppAndShortcuts(app, it.orEmpty()) }
        } ?: liveDataOf(null)
    }
    private val mutableSearchText = MutableLiveData<String?>()
    val searchText: LiveData<String?> = mutableSearchText
    private val searchTextAndShortcuts = searchText.combineWith(shortcuts) { searchText, shortcuts ->
        searchText to shortcuts
    }
    val apps = appRepository.getLiveLauncherApps().combineWith(searchTextAndShortcuts) { apps, searchTextAndShortcuts ->
        val searchText = searchTextAndShortcuts?.first
        val shortcuts = searchTextAndShortcuts?.second
        apps.orEmpty().map { app ->
            AppUiModel(
                app = app,
                isDisplayed = searchText?.let { app.name.contains(searchText, ignoreCase = true) } ?: app.isVisible,
                isSelected = shortcuts?.app == app,
                isGreyed = shortcuts?.let { it.app != app } ?: !app.isVisible,
            )
        }
    }

    fun onAppClick(app: App) {
        if (selectedApp.value == null || selectedApp.value == app) {
            launcherApps.startMainActivity(
                app.componentName,
                Process.myUserHandle(),
                null,
                null
            )
        } else {
            selectedApp.value = null
        }
    }

    fun onAppSelection(app: App) {
        if (selectedApp.value == null || selectedApp.value == app) {
            selectedApp.value = app
        } else {
            selectedApp.value = null
        }
    }

    @RequiresApi(Build.VERSION_CODES.N_MR1)
    fun onAppShortcutClick(shortcut: Shortcut) {
        selectedApp.value = null
        launcherApps.startShortcut(shortcut.packageName, shortcut.id, null, null, Process.myUserHandle())
    }

    fun onAppSettingsClick(app: App) {
        selectedApp.value = null
        launcherApps.startAppDetailsActivity(app.componentName, Process.myUserHandle(), null, null)
    }

    fun onAppVisibilityChange(app: App) {
        selectedApp.value = null
        viewModelScope.launch { appRepository.save(app.copy(isVisible = !app.isVisible)) }
    }

    fun startSearch() {
        if (mutableSearchText.value == null) {
            selectedApp.value = null
            mutableSearchText.value = ""
        }
    }

    fun setSearchText(searchText: String) {
        mutableSearchText.value = searchText
        selectedApp.value = null
    }

    fun onBack() {
        if (selectedApp.value != null) {
            selectedApp.value = null
        } else {
            mutableSearchText.value = null
        }
    }

    fun onCloseSearchClick() {
        mutableSearchText.value = null
    }
}

data class AppUiModel(
    val app: App,
    val isDisplayed: Boolean,
    val isSelected: Boolean,
    val isGreyed: Boolean
)

data class AppAndShortcuts(
    val app: App,
    val shortcuts: List<Shortcut>
)
