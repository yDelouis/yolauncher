package fr.ydelouis.yolauncher.ui.home

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.expandIn
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkOut
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.Indication
import androidx.compose.foundation.IndicationInstance
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.interaction.InteractionSource
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.PressInteraction
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Settings
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.drawscope.ContentDrawScope
import androidx.compose.ui.graphics.drawscope.scale
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.layout.Measurable
import androidx.compose.ui.layout.Placeable
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import dagger.hilt.android.AndroidEntryPoint
import fr.ydelouis.yolauncher.R
import fr.ydelouis.yolauncher.core.model.App
import fr.ydelouis.yolauncher.core.model.Shortcut
import fr.ydelouis.yolauncher.di.ViewModelFactory
import fr.ydelouis.yolauncher.ui.view.GridOptimizer
import fr.ydelouis.yolauncher.ui.view.YoLauncherTheme
import fr.ydelouis.yolauncher.util.asImageBitmap
import javax.inject.Inject
import kotlin.math.roundToInt


@AndroidEntryPoint
class HomeActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val viewModel by viewModels<HomeViewModel> { viewModelFactory }
    private var resumed = false

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        syncKeyboardWithSearch()
        setContent()
    }

    private fun setContent() = setContent {
        YoLauncherTheme {
            val apps: List<AppUiModel> by viewModel.apps.observeAsState(emptyList())
            val shortcuts by viewModel.shortcuts.observeAsState()
            val searchText by viewModel.searchText.observeAsState()
            Column {
                GridLayout(
                    modifier = Modifier.weight(weight = 1F, fill = true),
                    apps = apps,
                    appAndShortcuts = shortcuts,
                    onAppClick = { app -> viewModel.onAppClick(app) },
                    onAppLongClick = { app -> viewModel.onAppSelection(app) },
                    onShortcutClick = { shortcut -> viewModel.onAppShortcutClick(shortcut) },
                    onAppSettingsClick = { app -> viewModel.onAppSettingsClick(app) },
                    onAppVisibilityClick = { app -> viewModel.onAppVisibilityChange(app) },
                )
                SearchBar(
                    searchText = searchText,
                    onSearchTextChange = { newSearchText -> viewModel.setSearchText(newSearchText) },
                    onCloseSearch = { viewModel.onCloseSearchClick() }
                )
            }
        }
    }

    private fun syncKeyboardWithSearch() {
        viewModel.searchText.observe(this) { searchText ->
            if (searchText == null) hideKeyboard()
        }
    }

    private fun hideKeyboard() {
        val imm: InputMethodManager = getSystemService(InputMethodManager::class.java)
        val view = currentFocus ?: View(this)
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun onBackPressed() {
        viewModel.onBack()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if (Intent.ACTION_MAIN == intent?.action && resumed) {
            viewModel.startSearch()
        }
    }

    override fun onResume() {
        super.onResume()
        resumed = true
    }

    override fun onStop() {
        super.onStop()
        resumed = false
    }
}

private class AppsPlacer(
    val apps: List<AppUiModel>,
    val constraints: Constraints
) {
    private val displayedApps = apps.filter { it.isDisplayed }
    private val spec = GridOptimizer.getOptimizedSpec(
        width = constraints.maxWidth,
        height = constraints.maxHeight,
        itemCount = displayedApps.size,
        minColumnCount = 4
    )
    val appConstraints = Constraints.fixed(spec.squareSize, spec.squareSize)

    fun getSquarePlace(row: Int, column: Int): IntOffset {
        val s = spec.squareSize
        val spacingX = (constraints.maxWidth - (s * spec.columnCount)) / (spec.columnCount + 1)
        val spacingY = (constraints.maxHeight - (s * spec.rowCount)) / (spec.rowCount + 1)
        val x = spacingX + column * (s + spacingX)
        val y = if (displayedApps.size <= spec.columnCount) 0 else spacingY + row * (s + spacingY)
        return IntOffset(x, y)
    }

    fun getAppPlace(index: Int): IntOffset {
        val gridIndex = apps.take(index).count { it.isDisplayed }
        return getSquarePlace(row = gridIndex / spec.columnCount, column = gridIndex % spec.columnCount)
    }

    fun measureAndGetPopupPlace(app: App, measurable: Measurable): Pair<Placeable, IntOffset> {
        val index = apps.indexOfFirst { it.app == app }
        val appPlace = getAppPlace(index)
        val spaceAbove = appPlace.y
        val spaceBelow = constraints.maxHeight - appPlace.y - spec.squareSize
        val maxHeight = maxOf(spaceAbove, spaceBelow)
        val popupConstraints = Constraints(maxWidth = constraints.maxWidth, maxHeight = maxHeight)
        val popupPlaceable = measurable.measure(popupConstraints)
        val offsetXMax = constraints.maxWidth - popupPlaceable.width.toFloat()
        val preferredOffsetX = appPlace.x + (spec.squareSize - popupPlaceable.width) / 2F
        val offsetX = preferredOffsetX.coerceIn(0F, offsetXMax)
        val offsetYBelow = appPlace.y + spec.squareSize
        val offsetYAbove = appPlace.y - popupPlaceable.height
        val isEnoughSpaceBelow = popupPlaceable.height <= spaceBelow
        val offsetY = if (isEnoughSpaceBelow) offsetYBelow else offsetYAbove
        return popupPlaceable to IntOffset(offsetX.roundToInt(), offsetY)
    }
}

@Composable
fun GridLayout(
    modifier: Modifier = Modifier,
    apps: List<AppUiModel>,
    appAndShortcuts: AppAndShortcuts?,
    onAppClick: (App) -> Unit,
    onAppLongClick: (App) -> Unit,
    onShortcutClick: (Shortcut) -> Unit,
    onAppSettingsClick: (App) -> Unit,
    onAppVisibilityClick: (App) -> Unit
) {
    Layout(
        modifier = modifier,
        content = {
            apps.forEach { app ->
                AppComposable(
                    app,
                    onClick = { onAppClick(app.app) },
                    onLongClick = { onAppLongClick(app.app) }
                )
            }
            AppPopup(
                appAndShortcuts,
                onShortcutClick,
                onAppSettingsClick,
                onAppVisibilityClick
            )
        },
        measurePolicy = { measurables, constraints ->
        val appsPlacer = AppsPlacer(apps, constraints)
        val appPlaceables = measurables.take(apps.size).map { measurable ->
            measurable.measure(appsPlacer.appConstraints)
        }
        val popupPlaceableAndPlace = appAndShortcuts?.let { appsPlacer.measureAndGetPopupPlace(appAndShortcuts.app, measurables.last()) }

        layout(constraints.maxWidth, constraints.maxHeight) {
            appPlaceables.forEachIndexed { index, placeable -> placeable.placeRelative(appsPlacer.getAppPlace(index)) }
            popupPlaceableAndPlace?.let { (popupPlaceable, place) -> popupPlaceable.placeRelative(place) }
        }
    })
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun AppComposable(
    app: AppUiModel,
    onClick: () -> Unit,
    onLongClick: () -> Unit
) {
    val scale by animateFloatAsState(
        targetValue = when {
            !app.isDisplayed -> 0f
            app.isSelected -> 1.1f
            else -> 1f
        }
    )
    val alpha by animateFloatAsState(targetValue = if (app.isGreyed) .6F else 1F)
    Image(
        modifier = Modifier
            .combinedClickable(
                onClick = onClick,
                onLongClick = onLongClick,
                indication = AppIndication(),
                interactionSource = MutableInteractionSource()
            )
            .padding(2.dp)
            .scale(scale),
        bitmap = app.app.icon.asImageBitmap(),
        contentDescription = null,
        alpha = alpha,
    )
}

private class AppIndication : Indication {
    @Composable
    override fun rememberUpdatedInstance(interactionSource: InteractionSource): IndicationInstance {
        val instance = remember(interactionSource, this) { Instance() }
        LaunchedEffect(interactionSource, instance) {
            interactionSource.interactions.collect { interaction ->
                when (interaction) {
                    is PressInteraction.Press -> instance.startPress()
                    is PressInteraction.Release,
                    is PressInteraction.Cancel -> instance.endPress()
                }
            }
        }
        return instance
    }

    private class Instance : IndicationInstance {
        private val animatedScale = Animatable(1f)

        suspend fun startPress() {
            animatedScale.animateTo(1.1f)
        }

        suspend fun endPress() {
            animatedScale.animateTo(1f)
        }

        override fun ContentDrawScope.drawIndication() {
            scale(animatedScale.value) { this@drawIndication.drawContent() }
        }
    }
}

@Composable
fun AppPopup(
    appAndShortcuts: AppAndShortcuts?,
    onShortcutClick: (Shortcut) -> Unit,
    onSettingsClick: (App) -> Unit,
    onVisibilityClick: (App) -> Unit
) {
    AnimatedVisibility(
        visible = appAndShortcuts != null,
        enter = fadeIn() + expandIn { fullSize-> IntSize(fullSize.width, 0) },
        exit = fadeOut() + shrinkOut { fullSize -> IntSize(fullSize.width, 0) }
    ) {
        LazyColumn(
            modifier = Modifier
                .padding(4.dp)
                .background(color = MaterialTheme.colors.surface, shape = RoundedCornerShape(4.dp))
        ) {
            items(
                items = listOf(appAndShortcuts?.app?.name) + appAndShortcuts?.shortcuts.orEmpty() + appAndShortcuts?.app
            ) { item ->
                when (item) {
                    is String -> AppName(item)
                    is Shortcut -> ShortcutComposable(item, onShortcutClick)
                    is App -> AppSettingsComposable(item, onSettingsClick, onVisibilityClick)
                }
            }
        }
    }
}

@Composable
fun AppName(appName: String) {
    Text(
        modifier = Modifier.padding(horizontal = 16.dp, vertical = 8.dp),
        style = MaterialTheme.typography.h6,
        color = MaterialTheme.colors.onSurface,
        text = appName
    )
}

@Composable
fun ShortcutComposable(shortcut: Shortcut, onShortcutClick: (Shortcut) -> Unit) {
    Row(
        modifier = Modifier
            .clickable(onClick = { onShortcutClick(shortcut) })
            .padding(horizontal = 16.dp, vertical = 8.dp),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Image(
            bitmap = shortcut.icon.asImageBitmap(),
            contentDescription = null,
            modifier = Modifier.size(32.dp)
        )
        Text(
            modifier = Modifier.padding(start = 16.dp),
            color = MaterialTheme.colors.onSurface,
            text = shortcut.title?.toString().orEmpty()
        )
    }
}

@Composable
fun AppSettingsComposable(
    app: App,
    onSettingsClick: (App) -> Unit,
    onVisibilityClick: (App) -> Unit,
) {
    @Composable
    fun iconModifier(onClick: (App) -> Unit) =
        Modifier
            .clip(RoundedCornerShape(4.dp))
            .clickable(onClick = { onClick(app) })
            .padding(8.dp)
            .size(32.dp)
    Row(
        horizontalArrangement = Arrangement.SpaceEvenly,
        modifier = Modifier.padding(horizontal = 8.dp)
    ) {
        Icon(
            modifier = iconModifier(onSettingsClick),
            imageVector = Icons.Filled.Settings,
            tint = MaterialTheme.colors.onSurface,
            contentDescription = "Settings"
        )
        Icon(
            modifier = iconModifier(onVisibilityClick),
            painter = painterResource(if (app.isVisible) R.drawable.ic_visibility_off else R.drawable.ic_visibility),
            tint = MaterialTheme.colors.onSurface,
            contentDescription = "Visibility"
        )
    }
}

@Composable
fun SearchBar(
    searchText: String?,
    onSearchTextChange: (String) -> Unit,
    onCloseSearch: () -> Unit
) {
    AnimatedVisibility(
        visible = searchText != null,
        enter = slideInVertically { it },
        exit = slideOutVertically { it }
    ) {
        TextField(
            value = searchText.orEmpty(),
            onValueChange = onSearchTextChange,
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 16.dp)
                .background(
                    color = MaterialTheme.colors.surface,
                    shape = RoundedCornerShape(topStart = 4.dp, topEnd = 4.dp)
                ),
            colors = TextFieldDefaults.textFieldColors(textColor = MaterialTheme.colors.onSurface),
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
            keyboardActions = KeyboardActions(onDone = { onCloseSearch() }),
            trailingIcon = {
                IconButton(onClick = onCloseSearch) {
                    Icon(
                        imageVector = Icons.Filled.Close,
                        tint = MaterialTheme.colors.onSurface,
                        contentDescription = "Close"
                    )
                }
            }
        )
    }
}
