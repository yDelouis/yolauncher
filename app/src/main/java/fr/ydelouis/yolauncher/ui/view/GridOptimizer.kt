package fr.ydelouis.yolauncher.ui.view

import kotlin.math.ceil

object GridOptimizer {

    fun getOptimizedSpec(width: Int, height: Int, itemCount: Int, minColumnCount: Int = 1): GridSpec {
        if (itemCount == 0) return GridSpec(minColumnCount, 1, 0)
        var columnCount = minColumnCount - 1
        var size: Int
        do {
            columnCount++

            size = getIconSize(width, height, itemCount, columnCount)
            val columnsPlusOneSize =
                getIconSize(width, height, itemCount, columnCount + 1)
        } while (columnsPlusOneSize > size)
        return GridSpec(
            columnCount,
            getRowCount(itemCount, columnCount),
            size
        )
    }

    private fun getRowCount(itemCount: Int, columnCount: Int)
        = ceil(itemCount / columnCount.toFloat()).toInt()

    private fun getIconSize(width: Int, height: Int, itemCount: Int, columnCount: Int): Int {
        val rowCount = getRowCount(itemCount, columnCount)
        return minOf(width / columnCount, height / rowCount)
    }
}

data class GridSpec(
    val columnCount: Int,
    val rowCount: Int,
    val squareSize: Int
)
