package fr.ydelouis.yolauncher.di

import android.app.Application
import android.content.Context
import android.content.pm.LauncherApps
import androidx.room.Room
import dagger.Binds
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import fr.ydelouis.yolauncher.core.model.AppRepository
import fr.ydelouis.yolauncher.core.model.ShortcutRepository
import fr.ydelouis.yolauncher.data.AppRepositoryImpl
import fr.ydelouis.yolauncher.data.android.AndroidShortcutRepository
import fr.ydelouis.yolauncher.data.room.AppDatabase
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class AppModule {

    @Binds
    @Singleton
    abstract fun bindAppRepository(repo: AppRepositoryImpl): AppRepository

    @Binds
    abstract fun bindShortcutRepository(repo: AndroidShortcutRepository): ShortcutRepository

    companion object {

        @Provides
        fun provideLauncherApps(app: Application): LauncherApps =
            app.getSystemService(Context.LAUNCHER_APPS_SERVICE) as LauncherApps

        @Provides
        fun provideRoomDatabase(app: Application): AppDatabase {
            return Room.databaseBuilder(app, AppDatabase::class.java, "yoLauncher").build()
        }

        @Provides
        fun providePersistedAppDao(appDatabase: AppDatabase) = appDatabase.persistedAppDao()
    }
}
