package fr.ydelouis.yolauncher.data.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface PersistedAppDao {

    @Query("SELECT * FROM PersistedApp WHERE packageName IN (:packageNames)")
    fun getLivePersistedApps(packageNames: List<String>): LiveData<List<PersistedApp>>

    @Insert(entity = PersistedApp::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun save(persistedApp: PersistedApp)
}
