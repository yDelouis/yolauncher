package fr.ydelouis.yolauncher.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import androidx.lifecycle.switchMap
import fr.ydelouis.yolauncher.core.model.App
import fr.ydelouis.yolauncher.core.model.AppRepository
import fr.ydelouis.yolauncher.data.android.AndroidApp
import fr.ydelouis.yolauncher.data.android.AndroidAppRepository
import fr.ydelouis.yolauncher.data.room.PersistedApp
import fr.ydelouis.yolauncher.data.room.PersistedAppDao
import javax.inject.Inject

class AppRepositoryImpl @Inject constructor(
    private val androidAppRepository: AndroidAppRepository,
    private val persistedAppDao: PersistedAppDao
) : AppRepository {

    override fun getLiveLauncherApps(): LiveData<List<App>> {
        return androidAppRepository.liveLauncherAndroidApps.switchMap { androidApps ->
            val packageNames = androidApps.map { it.packageName }
            persistedAppDao.getLivePersistedApps(packageNames).map { persistedApps ->
                androidApps.map { androidApp ->
                    val persistedApp = persistedApps.find {
                        it.packageName == androidApp.packageName && it.activityName == androidApp.activityName
                    }
                    createApp(androidApp, persistedApp)
                }
            }
        }
    }

    private fun createApp(androidApp: AndroidApp, persistedApp: PersistedApp?) = App(
        name = androidApp.name,
        icon = androidApp.icon,
        componentName = androidApp.componentName,
        isVisible = persistedApp?.isVisible ?: true
    )

    override suspend fun save(app: App) {
        val persistedApp = PersistedApp(
            packageName = app.packageName,
            activityName = app.activityName,
            isVisible = app.isVisible
        )
        persistedAppDao.save(persistedApp)
    }
}
