package fr.ydelouis.yolauncher.data.android

import android.content.ComponentName
import android.graphics.drawable.Drawable

data class AndroidApp(
    val name: CharSequence,
    val icon: Drawable,
    val componentName: ComponentName
) {
    val packageName = componentName.packageName
    val activityName = componentName.className
}
