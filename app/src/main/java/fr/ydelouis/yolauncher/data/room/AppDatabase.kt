package fr.ydelouis.yolauncher.data.room

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [PersistedApp::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun persistedAppDao(): PersistedAppDao
}
