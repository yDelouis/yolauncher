package fr.ydelouis.yolauncher.data.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PersistedApp(
    val packageName: String,
    val activityName: String,
    @ColumnInfo(name = "isVisible") val isVisible: Boolean,
    @PrimaryKey val id: String = "$packageName////$activityName"
)
