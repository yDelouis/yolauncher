package fr.ydelouis.yolauncher.data.android

import android.content.pm.LauncherApps
import android.os.Process
import android.os.UserHandle
import androidx.lifecycle.LiveData
import fr.ydelouis.yolauncher.util.cancelChildren
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import java.util.Locale
import javax.inject.Inject

class AndroidAppRepository @Inject constructor(
    private val launcherApps: LauncherApps
) {

    val liveLauncherAndroidApps: LiveData<List<AndroidApp>> by lazy {
        object : LiveData<List<AndroidApp>>() {

            private val scope = CoroutineScope(Dispatchers.IO + SupervisorJob())
            private val launcherAppsCallback = object : LauncherApps.Callback() {
                override fun onPackagesUnavailable(packageNames: Array<out String>?, user: UserHandle?, replacing: Boolean) {
                    loadApps()
                }

                override fun onPackageChanged(packageName: String?, user: UserHandle?) {
                    loadApps()
                }

                override fun onPackagesAvailable(packageNames: Array<out String>?, user: UserHandle?, replacing: Boolean) {
                    loadApps()
                }

                override fun onPackageAdded(packageName: String?, user: UserHandle?) {
                    loadApps()
                }

                override fun onPackageRemoved(packageName: String?, user: UserHandle?) {
                    loadApps()
                }
            }

            override fun onActive() {
                loadApps()
                launcherApps.registerCallback(launcherAppsCallback)
            }

            override fun onInactive() {
                scope.cancelChildren()
                launcherApps.unregisterCallback(launcherAppsCallback)
            }

            private fun loadApps() = scope.launch { postValue(getLauncherAndroidApps()) }
        }
    }

    private fun getLauncherAndroidApps(): List<AndroidApp> {
        val activities = launcherApps.getActivityList(null, Process.myUserHandle())
        return activities.map {
            AndroidApp(
                name = it.label,
                icon = it.getBadgedIcon(0),
                componentName = it.componentName
            )
        }.sortedBy { it.name.toString().lowercase() }
    }
}
