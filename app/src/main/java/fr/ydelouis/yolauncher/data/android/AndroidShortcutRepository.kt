package fr.ydelouis.yolauncher.data.android

import android.content.pm.LauncherApps
import android.content.pm.LauncherApps.ShortcutQuery.FLAG_MATCH_DYNAMIC
import android.content.pm.LauncherApps.ShortcutQuery.FLAG_MATCH_MANIFEST
import android.os.Build
import android.os.Process
import androidx.annotation.RequiresApi
import androidx.lifecycle.LiveData
import fr.ydelouis.yolauncher.core.model.Shortcut
import fr.ydelouis.yolauncher.core.model.ShortcutRepository
import fr.ydelouis.yolauncher.util.cancelChildren
import fr.ydelouis.yolauncher.util.liveDataOf
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import javax.inject.Inject

class AndroidShortcutRepository @Inject constructor(private val launcherApps: LauncherApps) : ShortcutRepository {

    override fun getLiveAppShortcuts(packageName: String): LiveData<List<Shortcut>?> {
        return if (Build.VERSION.SDK_INT < 25) {
            liveDataOf(null)
        } else {
            object : LiveData<List<Shortcut>?>() {

                private val scope = CoroutineScope(Dispatchers.IO + SupervisorJob())

                override fun onActive() {
                    scope.launch { postValue(getAppShortcuts(packageName)) }
                }

                override fun onInactive() {
                    scope.cancelChildren()
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.N_MR1)
    private fun getAppShortcuts(packageName: String): List<Shortcut>? {
        val query = LauncherApps.ShortcutQuery()
            .setPackage(packageName)
            .setQueryFlags(FLAG_MATCH_MANIFEST or FLAG_MATCH_DYNAMIC)
        return try {
            launcherApps.getShortcuts(query, Process.myUserHandle())?.map {
                val icon = launcherApps.getShortcutIconDrawable(it, 0)
                Shortcut(it.id, it.shortLabel ?: it.longLabel, icon, it.`package`)
            }
        } catch (exception: SecurityException) {
            null
        }
    }
}
