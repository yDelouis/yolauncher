buildscript {
    repositories {
        google()
    }
    dependencies {
        classpath(libs.android.gradle)
        classpath(libs.hilt.gradle)
    }
}

plugins {
    kotlin("android") version libs.versions.kotlin apply false
}

subprojects {
    repositories {
        mavenCentral()
        google()
        maven("https://dl.bintray.com/kotlin/kotlinx")
    }
}
